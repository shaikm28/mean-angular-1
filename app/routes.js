var {Nerd} = require('./models/nerd');
var path = require('path');

module.exports = function (app) {
    app.get('/api/nerds', (req, res) => {
        Nerd.find().then((nerds) => {
            console.log(`REST API response: nerds : ${nerds}`);
            res.send({ nerds });
        }).catch((err) => {
            console.log(err);
            res.status(400).send();
        });
    });

    app.post('/api/nerds', (req, res) => {
        var nerd = new Nerd( {
            name: 'shaik m 1'
        });
        nerd.save().then((nerd)=> {
            console.log('Saved nerd: ', nerd);
            return res.send({nerd});
        }).catch((err) => {
            console.log('Unable to save nerd: ', err);
            return res.status(400).send();
        })
    });

    app.get('*', (req, res) => {
        res.sendFile('views/index.html', {root : path.join(__dirname, '../public/')});
    });
}
