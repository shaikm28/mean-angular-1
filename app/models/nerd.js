var mongoose = require('mongoose');

var nerdSchema = new mongoose.Schema({
    name: {
        type: String,
        default: 'abc'
    }
});

var Nerd = mongoose.model('Nerd', nerdSchema);

module.exports = {
    Nerd
}