angular.module('NerdService', []).factory('Nerd', ['$http', ($http) => {
    return {
        get: () => {
            console.log('inside nerd service, get() ');
            return $http.get('/api/nerds');
        },
        create: (nerdData) => {
            return $http.post('/api/nerds', nerdData);
        },
        delete: (id) => {
            return $http.delete('/api/nerds/' + id);
        }
    }
}]);