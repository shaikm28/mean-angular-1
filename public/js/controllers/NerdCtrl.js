angular.module('NerdCtrl', []).controller('NerdController', function($scope, $http, Nerd) {
    // $http.get('http://localhost:8080/api/nerds').then((res) => {
    //     $scope.tagline = res.data;
    // });
    console.log('inside nerd controller');
    Nerd.get().then((res) => {
        console.log('res:', res.data);
        $scope.tagline = res.data;
    });
    // $scope.tagline = 'From Nerd controller';
});